#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "$SSH_KEY_4" > key.pem
chmod 400 key.pem

if [ "$1" == "BUILD" ];then
echo '[*] Building Program To Docker Images'
echo "[*] Tag $WAKTU"
docker build -t cahyoarissabarno/kube-pipeline:$CI_COMMIT_BRANCH .
docker login --username=$DOCKER_USER --password=$DOCKER_PASS
docker push cahyoarissabarno/kube-pipeline:$CI_COMMIT_BRANCH
echo $CI_PIPELINE_ID

elif [ "$1" == "DEPLOY" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
echo '[*] Execute Remote SSH'
# ssh -i key.pem -o "StrictHostKeyChecking no" root@demo1.bisa.ai -p 22 "microk8s.kubectl delete -f deployment.yaml"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "export PATH=$PATH:/snap/bin"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "cat .bashrc"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "echo $PATH"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "echo copy yaml file ...."
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "cp template.yaml deployment-kel-7.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "echo write to yaml file ...."
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "sed -i 's/IMAGEPRODUCT/cahyoarissabarno\/kube-pipeline:$CI_COMMIT_BRANCH/g' deployment-kel-7.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "sed -i 's/NAME/kubecicd7/g' deployment-kel-7.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "sed -i 's/REPNUM/2/g' deployment-kel-7.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "sed -i 's/NODEPORT/30003/g' deployment-kel-7.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "echo run yaml file ...."
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.199.27 -p 22 "microk8s.kubectl apply -f deployment-kel-7.yaml"
echo $CI_PIPELINE_ID
fi
